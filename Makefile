all: clean gpstest gpstest-debug

clean:
	rm -f gpstest gpstest-debug

# -lmvec is necessary for -Ofast
gpstest:
	gcc -pthread -lm -lmvec -lairspy -Wall -Wextra -Wno-unused-parameter -g -Ofast *.c -o gpstest

gpstest-debug:
	gcc -pthread -lm -lairspy -Wall -Wextra -Wno-unused-parameter -g -Og *.c -o gpstest-debug
