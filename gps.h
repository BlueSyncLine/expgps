#ifndef GPS_H
#define GPS_H
#define GPS_CHIP_RATE 1.023e6f
#define GPS_SEQUENCE_LENGTH 1023
#define GPS_SEQUENCES_PER_BIT 20
#define GPS_BITS_PER_WORD 30
#define GPS_SUBFRAME_PREAMBLE 0x8b
#define GPS_WORDS_PER_SUBFRAME 10
#define GPS_SECONDS_PER_SUBFRAME 6
#define GPS_SECONDS_PER_WORD 0.6f
#define GPS_SECONDS_PER_TOW 6.0f

enum {
    GPS_SUBFRAME_INVALID,
    GPS_SUBFRAME_1,
    GPS_SUBFRAME_2,
    GPS_SUBFRAME_3,
    GPS_SUBFRAME_4,
    GPS_SUBFRAME_5
};

typedef struct {
    int weekNumber;
    int l2Code;
    int uraIndex;
    int svHealth;
    int iodc;
    int l2PFlag;
    double t_gd;
    double t_oc;
    double a_f2;
    double a_f1;
    double a_f0;
} gps_subframe_1_t;

typedef struct {
    int iode;
    double c_rs;
    double delta_n;
    double m_0;
    double c_uc;
    double e;
    double c_us;
    double sqrt_a;
    double t_oe;
    int fitIntervalFlag;
    int aodo;
} gps_subframe_2_t;

typedef struct {
    double c_ic;
    double an_0;
    double c_is;
    double i_0;
    double c_rc;
    double arg;
    double an_dot;
    int iode;
    double i_dot;
} gps_subframe_3_t;

// XXX implement
typedef struct {} gps_subframe_4_t;
typedef struct {} gps_subframe_5_t;

typedef struct {
    int integrityStatusFlag;
    int towTruncated;
    int antiSpoofFlag;
    int alertFlag;
    int subframeId;

    union {
        gps_subframe_1_t subframe1;
        gps_subframe_2_t subframe2;
        gps_subframe_3_t subframe3;
        gps_subframe_4_t subframe4;
        gps_subframe_5_t subframe5;
    } fields;
} gps_subframe_t;

void gps_generatePRN(int *sequence, int prn);
int gps_decodeWord(unsigned int word);
void gps_decodeSubframe(gps_subframe_t *subframe, int words[GPS_WORDS_PER_SUBFRAME]);
int gps_findTimeCorrection(double *seconds, gps_subframe_t *subframe1);
#endif
