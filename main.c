#include <signal.h>
#include <unistd.h> // sleep
#include <string.h> // memcpy
#include <pthread.h>
#include <libairspy/airspy.h>
#include <math.h>
#include <complex.h>
#include <assert.h>
#include "rx.h"
#include "fir.h"
#include "gps.h"
#include "navigation.h"
#include "ephemeris.h"
#include "log.h"

#define INPUT_RATE 2500000
#define SENSITIVITY_GAIN 15
#define GPS_FREQUENCY 1575420000

// Empirical.
#define STRENGTH_THRESHOLD 4.0f

// Allow at least a minute for acquisition.
#define ACQUISITION_DELAY 60

pthread_t evaluationThreadId;
pthread_cond_t evaluationPendingCond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t evaluationPendingMutex = PTHREAD_MUTEX_INITIALIZER;
int evaluationPending, evaluationThreadCreated, evaluationInitialComplete;
struct airspy_device *airspyDevice;

#define OVERSAMPLING 5
#define RESAMPLED_RATE (INPUT_RATE * OVERSAMPLING)
#define FILTER_N 31
#define FILTER_LENGTH (FILTER_N * 2 + 1)
float complex resampledBuffer[RX_EVALUATION_WINDOW_LENGTH], evaluationBuffer[RX_EVALUATION_WINDOW_LENGTH], filterDelay[FILTER_LENGTH], *rxRotationTable;
float filterTaps[FILTER_LENGTH];
int resampledBufferIndex, filterIndex;
float resamplingPhase;
int exiting;

#define NUM_PRNS 32
float complex rxSamples[RX_EVALUATION_WINDOW_LENGTH];
pthread_t rxThreads[NUM_PRNS];

typedef struct {
    int channelIndex;
} channelSampleThread_param_t;
channelSampleThread_param_t rxThreadParams[NUM_PRNS];

typedef struct {
    int prn;
    int subframe1Valid;
    int subframe2Valid;
    int subframe3Valid;
    gps_subframe_t subframe1;
    gps_subframe_t subframe2;
    gps_subframe_t subframe3;
    long long subframe3Offset;
} subframe_tuple_t;
subframe_tuple_t navigationSubframeTuples[NUM_PRNS];

// We use multiple condition variables because as threads finish work they'll loop back and that seemed like a way to avoid a race condition when the variable is reset.
pthread_mutex_t rxChannelMutexes[NUM_PRNS], rxSamplesReadyMutexes[NUM_PRNS], rxSubframeMutex = PTHREAD_MUTEX_INITIALIZER, rxNumPendingChannelsMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t rxSamplesReadyConds[NUM_PRNS], rxNumPendingChannelsCond = PTHREAD_COND_INITIALIZER;
int rxThreadCreated[NUM_PRNS], rxSamplesReady[NUM_PRNS], rxNumPendingChannels;
gps_rx_channel_t rxChannels[NUM_PRNS];

void *evaluationThread(void *arg) {
    float signalStrength, frequencyOffset;
    int evaluationPRN, sequencePhase;

    // Set the cancellation type so that the thread can be stopped instantly.
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    // Start from the first PRN.
    evaluationPRN = 1;

    while (1) {
        // Wait for a signal that the buffer is ready.
        // It's necessary to verify the variable in order to avoid spurious condition signals.
        pthread_mutex_lock(&evaluationPendingMutex);
        while (!evaluationPending)
            pthread_cond_wait(&evaluationPendingCond, &evaluationPendingMutex);
        pthread_mutex_unlock(&evaluationPendingMutex);

        // Evaluate the received window for this PRN.
        gps_rx_evaluateWindow(rxRotationTable, evaluationBuffer, evaluationPRN, &signalStrength, &frequencyOffset, &sequencePhase);
        log_printf("PRN %d: strength %f, offset %f, seq %d\n", evaluationPRN, signalStrength, frequencyOffset, sequencePhase);

        // If we don't have sync on this channel, reset.
        pthread_mutex_lock(&rxChannelMutexes[evaluationPRN - 1]);
        if (!evaluationInitialComplete || !gps_rx_channel_getFrameSync(&rxChannels[evaluationPRN - 1])) {
            if (signalStrength > STRENGTH_THRESHOLD) {
                log_printf("PRN %d: reset\n", evaluationPRN);
                gps_rx_channel_reset(&rxChannels[evaluationPRN - 1], evaluationPRN, frequencyOffset, sequencePhase);
            } else {
                log_printf("PRN %d: deactivated\n", evaluationPRN);
                gps_rx_channel_disable(&rxChannels[evaluationPRN - 1]);
            }
        }
        pthread_mutex_unlock(&rxChannelMutexes[evaluationPRN - 1]);

        // Roll over the PRN number.
        if (++evaluationPRN == NUM_PRNS + 1) {
            evaluationPRN = 1;

            // If we haven't done an evaluation yet, this concludes it.
            if (!evaluationInitialComplete)
                evaluationInitialComplete = 1;

            // Sleep, allowing the receiver channels to synchronize.
            sleep(ACQUISITION_DELAY);
        }

        // Reset the flag.
        pthread_mutex_lock(&evaluationPendingMutex);
        evaluationPending = 0;
        pthread_mutex_unlock(&evaluationPendingMutex);
    }
}

void *channelSampleThread(void *arg) {
    int targetIndex;
    channelSampleThread_param_t *param;

    // Allow instant cancellation.
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    param = (channelSampleThread_param_t*)arg;
    targetIndex = param->channelIndex;

    while(1) {
        // Wait until the samples become available.
        pthread_mutex_lock(&rxSamplesReadyMutexes[targetIndex]);
        while (!rxSamplesReady[targetIndex])
            pthread_cond_wait(&rxSamplesReadyConds[targetIndex], &rxSamplesReadyMutexes[targetIndex]);

        // Reset the flag.
        rxSamplesReady[targetIndex] = 0;
        pthread_mutex_unlock(&rxSamplesReadyMutexes[targetIndex]);

        // Feed samples into the channel.
        pthread_mutex_lock(&rxChannelMutexes[targetIndex]);
        gps_rx_channel_input(&rxChannels[targetIndex], rxSamples, RX_EVALUATION_WINDOW_LENGTH);
        pthread_mutex_unlock(&rxChannelMutexes[targetIndex]);

        // Decrement the counter and signal if done.
        pthread_mutex_lock(&rxNumPendingChannelsMutex);
        if (--rxNumPendingChannels == 0)
            pthread_cond_signal(&rxNumPendingChannelsCond);
        pthread_mutex_unlock(&rxNumPendingChannelsMutex);
    }
}

// Callback for received samples.
int sampleCallback(airspy_transfer_t *transfer) {
    int i, j, k, n;
    float complex *samples, accumulator;

    assert(transfer->sample_type == AIRSPY_SAMPLE_FLOAT32_IQ);
    samples = (float complex *)transfer->samples;

    if (transfer->dropped_samples)
        log_printf("!!! dropped samples: %lu\n", transfer->dropped_samples);

    for (i = 0; i < transfer->sample_count; i++) {
        for (j = 0; j < OVERSAMPLING; j++) {
            filterDelay[filterIndex++] = j == 0 ? samples[i] : 0.0f;
            filterIndex %= FILTER_LENGTH;

            resamplingPhase += RX_RATE / RESAMPLED_RATE;

            // Sampling point.
            if (resamplingPhase >= 1.0f) {
                // Roll over.
                resamplingPhase -= 1.0f;

                // Compute the average at that time.
                accumulator = 0.0f;
                for (k = 0; k < FILTER_LENGTH; k++)
                    accumulator += filterTaps[k] * filterDelay[(filterIndex + k) % FILTER_LENGTH];

                // Place into the buffer.
                resampledBuffer[resampledBufferIndex++] = accumulator;

                // Full window!
                if (resampledBufferIndex == RX_EVALUATION_WINDOW_LENGTH) {
                    resampledBufferIndex = 0;

                    // Inform the evaluation thread if it's not busy (skip if it is).
                    pthread_mutex_lock(&evaluationPendingMutex);
                    if (!evaluationPending) {
                        // Set the flag.
                        evaluationPending = 1;

                        // Copy the samples over.
                        memcpy(evaluationBuffer, resampledBuffer, RX_EVALUATION_WINDOW_LENGTH * sizeof(float complex));

                        // Inform the thread.
                        pthread_cond_signal(&evaluationPendingCond);
                    }
                    pthread_mutex_unlock(&evaluationPendingMutex);

                    // Are there any pending channel threads? If yes, wait until they all finish.
                    pthread_mutex_lock(&rxNumPendingChannelsMutex);
                    while (rxNumPendingChannels)
                        pthread_cond_wait(&rxNumPendingChannelsCond, &rxNumPendingChannelsMutex);

                    // Reset so that all channels are pending again.
                    rxNumPendingChannels = NUM_PRNS;
                    pthread_mutex_unlock(&rxNumPendingChannelsMutex);

                    // Copy the buffer over.
                    memcpy(rxSamples, resampledBuffer, RX_EVALUATION_WINDOW_LENGTH * sizeof(float complex));

                    // Inform all threads that there are samples available.
                    for (n = 0; n < NUM_PRNS; n++) {
                        pthread_mutex_lock(&rxSamplesReadyMutexes[n]);
                        rxSamplesReady[n] = 1;
                        pthread_cond_signal(&rxSamplesReadyConds[n]);
                        pthread_mutex_unlock(&rxSamplesReadyMutexes[n]);
                    }
                }
            }
        }
    }

    // Any other return value stops reception.
    return 0;
}

// This is supposed to be called from the subframe callback with the mutex acquired.
void processNavigation(int latestTOW) {
    int i, numValidTuples, subframe3TOW;
    long long minOffset;
    double timeCorrection, currentTime, timeOffset;
    subframe_tuple_t *subframeTuple, *validTuples[NUM_PRNS];
    gps_navigation_satellitePosition_t satellitePositions[NUM_PRNS];
    gps_navigation_solution_t solution;
    ECEF_coords_t satelliteCoords;
    LLA_coords_t latlon;

    numValidTuples = 0;

    // The only valid subframes are ones sharing the same TOW value.
    for (i = 0; i < NUM_PRNS; i++) {
        subframeTuple = &navigationSubframeTuples[i];

        if (subframeTuple->subframe1Valid && subframeTuple->subframe2Valid && subframeTuple->subframe3Valid) {
            subframe3TOW = subframeTuple->subframe3.towTruncated;

            // Store the current tuple as valid.
            if (subframe3TOW == latestTOW)
                validTuples[numValidTuples++] = subframeTuple;
        }
    }

    log_printf("processNavigation: %d valid subframe tuples.\n", numValidTuples);

    // No data, nothing to do.
    if (numValidTuples < NAVIGATION_MIN_SATELLITES) {
        log_printf("Not enough satellites for computing the location.\n");
        return;
    }

    // Find the minimum RX time offset for normalization.
    minOffset = -1;
    for (i = 0; i < numValidTuples; i++) {
        if (minOffset == -1 || validTuples[i]->subframe3Offset < minOffset)
            minOffset = validTuples[i]->subframe3Offset;
    }

    // Compute satellite positions.
    for (i = 0; i < numValidTuples; i++) {
        subframeTuple = validTuples[i];
        currentTime = subframeTuple->subframe3.towTruncated * GPS_SECONDS_PER_TOW;
        if (!gps_findTimeCorrection(&timeCorrection, &subframeTuple->subframe1)) {
            log_printf("Failed to compute the time correction value!\n");
            return;
        }

        log_printf("Time correction: %.12f\n", timeCorrection);

        if (!gps_ephemeris_findPosition(&satelliteCoords, &subframeTuple->subframe2, &subframeTuple->subframe3, currentTime - timeCorrection)) {
            log_printf("Failed to compute a satellite position!\n");
            return;
        }

        // Compute the time offset.
        // Note: the correction value is added and not subtracted here, because a positive offset leads to an earlier sending of the signal.
        timeOffset = (validTuples[i]->subframe3Offset - minOffset) / RX_RATE + timeCorrection;

        // Store the value.
        satellitePositions[i].coords = satelliteCoords;
        satellitePositions[i].timeOffset = timeOffset;
    }

    // Try solving the navigation equation and obtaining our coordinates.
    if (!gps_navigation_solve(&solution, satellitePositions, numValidTuples)) {
        log_printf("Navigation solution failed\n");
        return;
    }

    // Solve.
    gps_navigation_ECEFtoLLA(&latlon, &solution.coords);

    // Output.
    printf("coordinates: lat %f, lon %f, alt %f; ", latlon.latitude, latlon.longitude, latlon.altitude);
    printf("PRNs: ");
    for (i = 0; i < numValidTuples; i++)
        printf("%d, ", validTuples[i]->prn);
    printf("\n");
}

// Subframe callback.
void subframeCallback(int prn, gps_subframe_t subframe, long long rxSampleOffset) {
    subframe_tuple_t *subframeTuple;

    // Lock the mutex.
    pthread_mutex_lock(&rxSubframeMutex);

    // The subframe tuple corresponding to the current PRN.
    subframeTuple = &navigationSubframeTuples[prn - 1];

    switch (subframe.subframeId) {
        case GPS_SUBFRAME_1:
            log_printf("Subframe 1 from PRN %d acquired.\n", prn);
            subframeTuple->subframe1 = subframe;
            subframeTuple->subframe1Valid = 1;
            break;
        case GPS_SUBFRAME_2:
            log_printf("Subframe 2 from PRN %d acquired.\n", prn);
            subframeTuple->subframe2 = subframe;
            subframeTuple->subframe2Valid = 1;
            break;
        case GPS_SUBFRAME_3:
            log_printf("Subframe 3 from PRN %d acquired.\n", prn);
            subframeTuple->subframe3 = subframe;
            subframeTuple->subframe3Valid = 1;
            subframeTuple->subframe3Offset = rxSampleOffset;

            // Process.
            processNavigation(subframeTuple->subframe3.towTruncated);

            /*log_printf("a_f0: %f\n", subframeTuple->subframe1.fields.subframe1.a_f0);
            log_printf("a_f1: %f\n", subframeTuple->subframe1.fields.subframe1.a_f1);
            log_printf("a_f2: %f\n", subframeTuple->subframe1.fields.subframe1.a_f2);

            // The necessary subframes are around.
            if (subframeTuple->subframe1Valid && subframeTuple->subframe2Valid && subframeTuple->subframe3Valid && gps_ephemeris_findPosition(&ecefCoords, &subframeTuple->subframe2, &subframeTuple->subframe3, subframe.towTruncated * GPS_SECONDS_PER_TOW)) {
                // Convert the coordinates.
                gps_navigation_ECEFtoLLA(&coords, &ecefCoords);

                log_printf("At the start of the subframe, PRN %d was at %f %f %f\n", prn, ecefCoords.x, ecefCoords.y, ecefCoords.z);
                log_printf("lat=%f lon=%f alt=%f\n", coords.latitude, coords.longitude, coords.altitude);
            }*/
            break;
    }

    // Unlock the mutex.
    pthread_mutex_unlock(&rxSubframeMutex);
}

void quit(int exitCode) {
    int i;

    // We don't expect this to be called twice but it might happen.
    if (exiting)
        return;

    exiting = 1;

    // Stop callbacks.
    if (airspyDevice)
        airspy_stop_rx(airspyDevice);

    // Terminate the thread.
    if (evaluationThreadCreated) {
        pthread_cancel(evaluationThreadId);
        pthread_join(evaluationThreadId, NULL);
    }

    for (i = 0; i < NUM_PRNS; i++) {
        // Only applies if the thread for this PRN exists.
        if (!rxThreadCreated[i])
            continue;

        // Stop the thread.
        pthread_cancel(rxThreads[i]);
        pthread_join(rxThreads[i], NULL);

        // Get rid of all its mutexes and conditions.
        pthread_mutex_unlock(&rxChannelMutexes[i]);
        pthread_mutex_destroy(&rxChannelMutexes[i]);
        pthread_mutex_unlock(&rxSamplesReadyMutexes[i]);
        pthread_mutex_destroy(&rxSamplesReadyMutexes[i]);
        pthread_cond_destroy(&rxSamplesReadyConds[i]);
    }

    // Unlock and destroy the mutex and the condition variables.
    pthread_mutex_unlock(&evaluationPendingMutex);
    pthread_mutex_destroy(&evaluationPendingMutex);
    pthread_cond_destroy(&evaluationPendingCond);
    pthread_mutex_unlock(&rxNumPendingChannelsMutex);
    pthread_mutex_destroy(&rxNumPendingChannelsMutex);
    pthread_cond_destroy(&rxNumPendingChannelsCond);

    // Free the memory.
    free(rxRotationTable);

    // Works even if NULL.
    airspy_close(airspyDevice);

    exit(exitCode);
}

// Signal handler.
void signalHandler(int signum) {
    log_printf("Signal caught, stopping!\n");
    quit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    int i, err;

    // Initialize the variables.
    evaluationPending = 0;
    evaluationThreadCreated = 0;
    evaluationInitialComplete = 0;

    for (i = 0; i < FILTER_LENGTH; i++)
        filterDelay[i] = 0.0f;
    filterIndex = 0;
    resampledBufferIndex = 0;
    resamplingPhase = 0.0f;
    exiting = 0;

    // Compute the filter taps.
    fir_lowpass_taps(filterTaps, FILTER_N, GPS_CHIP_RATE / RESAMPLED_RATE);

    for (i = 0; i < NUM_PRNS; i++) {
        // Initialize all the channel structures.
        gps_rx_channel_init(&rxChannels[i], subframeCallback);
        navigationSubframeTuples[i].prn = i + 1;
        navigationSubframeTuples[i].subframe1Valid = 0;
        navigationSubframeTuples[i].subframe2Valid = 0;
        navigationSubframeTuples[i].subframe3Valid = 0;

        // Initialize the thread stuff.
        pthread_mutex_init(&rxChannelMutexes[i], NULL);
        pthread_mutex_init(&rxSamplesReadyMutexes[i], NULL);
        pthread_cond_init(&rxSamplesReadyConds[i], NULL);
        rxSamplesReady[i] = 0;
        rxThreadCreated[i] = 0;
    }

    // Register signal handlers.
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    if (!(rxRotationTable = malloc(RX_ROTATION_TABLE_LENGTH * sizeof(float complex)))) {
        perror("Couldn't allocate memory");
        quit(EXIT_FAILURE);
    }

    // Generate the rotation table.
    gps_rx_generateRotationTable(rxRotationTable);

    // Try creating the sample evaluation thread.
    if (pthread_create(&evaluationThreadId, NULL, evaluationThread, NULL)) {
        perror("Couldn't create a thread");
        quit(EXIT_FAILURE);
    }

    // We can now use pthread_cancel.
    evaluationThreadCreated = 1;

    // Create the worker threads for all PRNs.
    for (i = 0; i < NUM_PRNS; i++) {
        rxThreadParams[i].channelIndex = i;
        if (pthread_create(&rxThreads[i], NULL, channelSampleThread, &rxThreadParams[i])) {
            perror("Couldn't create a thread");
            quit(EXIT_FAILURE);
        }

        rxThreadCreated[i] = 1;
    }

    // Try opening the device.
    if ((err = airspy_open(&airspyDevice))) {
        log_printf("Couldn't open the Airspy device: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Set the sample rate.
    if ((err = airspy_set_samplerate(airspyDevice, INPUT_RATE))) {
        log_printf("Couldn't set the sample rate: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Set the frequency.
    if ((err = airspy_set_freq(airspyDevice, GPS_FREQUENCY))) {
        log_printf("Couldn't set the frequency: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Set the sample type.
    if ((err = airspy_set_sample_type(airspyDevice, AIRSPY_SAMPLE_FLOAT32_IQ))) {
        log_printf("Couldn't set the sample type: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Set the gain.
    if ((err = airspy_set_sensitivity_gain(airspyDevice, SENSITIVITY_GAIN))) {
        log_printf("Couldn't set the gain: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Enable the bias tee.
    if ((err = airspy_set_rf_bias(airspyDevice, 1))) {
        log_printf("Couldn't enable the bias tee: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Start reception.
    if ((err = airspy_start_rx(airspyDevice, sampleCallback, NULL))) {
        log_printf("Couldn't start reception: %s\n", airspy_error_name(err));
        quit(EXIT_FAILURE);
    }

    // Sleep.
    while (airspy_is_streaming(airspyDevice))
        sleep(1);

    log_printf("The receiver has stopped streaming.\n");
    quit(EXIT_SUCCESS);
}
