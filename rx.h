#ifndef RX_H
#define RX_H
#include <complex.h>
#include "gps.h"

#define RX_SAMPLES_PER_CHIP 3
#define RX_WINDOW_LENGTH (GPS_SEQUENCE_LENGTH * RX_SAMPLES_PER_CHIP)

// XXX has to be large enough or samples may get dropped (this value is shared as the buffer size)
#define RX_EVALUATION_WINDOW_LENGTH (RX_WINDOW_LENGTH * 2)
#define RX_RATE (GPS_CHIP_RATE * RX_SAMPLES_PER_CHIP)
#define RX_DOPPLER_BINS 21
#define RX_DOPPLER_BIN_WIDTH 500.0f
#define RX_SYNC_EDGE_REPEAT_THRESHOLD 10

//#define RX_PHASE_COEFF 0.01f
//#define RX_FREQUENCY_COEFF 0.1f
#define RX_PHASE_COEFF 0.01f
#define RX_FREQUENCY_COEFF 0.1f // Hz/rad

#define RX_ROTATION_TABLE_LENGTH (RX_DOPPLER_BINS * RX_EVALUATION_WINDOW_LENGTH)
#define RX_PARITY_ERROR_THRESHOLD 10
#define RX_DUMMY_PRN 0

// Callback invoked on every subframe. rxSampleOffset is counted at RX_RATE.
typedef void gps_rx_subframeCallback(int prn, gps_subframe_t subframe, long long rxSampleOffset);

typedef struct {
    int prn;
    int prnSequence[GPS_SEQUENCE_LENGTH];
    float complex correlationWindow[RX_WINDOW_LENGTH];
    int windowIndex;
    float frequencyOffset;
    float rotationPhase;

    int syncOffset;
    int syncCounter;
    float syncEarlyMagnitude;
    float syncLateMagnitude;
    float syncCenterMagnitude;
    int syncSkipAdjustment;
    float complex peakSample;
    int skipCounter;

    float symbolAverageWindow[GPS_SEQUENCES_PER_BIT];
    int symbolAverageIndex;
    int bitClockCounter;
    float syncBitClockPrevPeakReal;
    int frameSync;
    unsigned int wordShift;
    int wordBitCounter;

    long long sampleCounter;

    int parityErrorCounter;
    int syncPrevBitClockCounter;
    int syncEdgeRepeatCounter;

    int subframeWordCounter;
    int subframeWords[GPS_WORDS_PER_SUBFRAME];

    gps_rx_subframeCallback *subframeCallback;
} gps_rx_channel_t;

void gps_rx_generateRotationTable(float complex *rotationTable);
void gps_rx_evaluateWindow(float complex *rotationTable, float complex *window, int prn, float *signalStrength, float *frequencyOffset, int *sequencePhase);

void gps_rx_channel_init(gps_rx_channel_t *channel, gps_rx_subframeCallback *subframeCallback);
void gps_rx_channel_disable(gps_rx_channel_t *channel);
void gps_rx_channel_reset(gps_rx_channel_t *channel, int prn, float initialFrequencyOffset, int initialPhaseOffset);
void gps_rx_channel_input(gps_rx_channel_t *channel, float complex *samples, int numSamples);
long long gps_rx_channel_getSamplesSinceInit(gps_rx_channel_t *channel);
int gps_rx_channel_getFrameSync(gps_rx_channel_t *channel);
#endif
