#include <math.h>
#include <complex.h>
#include "rx.h"
#include "gps.h"
#include "log.h"

#define DEBUG_PRN 0

// Generates the rotation table for correlation with Doppler offsets.
void gps_rx_generateRotationTable(float complex *rotationTable) {
    int dopplerBin, t;
    float frequencyOffset;
    for (dopplerBin = 0; dopplerBin < RX_DOPPLER_BINS; dopplerBin++) {
        frequencyOffset = (dopplerBin - RX_DOPPLER_BINS / 2) * RX_DOPPLER_BIN_WIDTH;

        for (t = 0; t < RX_EVALUATION_WINDOW_LENGTH; t++)
            rotationTable[dopplerBin * RX_EVALUATION_WINDOW_LENGTH + t] = cexpf(-I * frequencyOffset * t * M_PI * 2.0 / RX_RATE);
    }
}

// Evaluate a window of samples for a given PRN number.
void gps_rx_evaluateWindow(float complex *rotationTable, float complex *window, int prn, float *signalStrength, float *frequencyOffset, int *sequencePhase) {
    int i, prnSequence[GPS_SEQUENCE_LENGTH], dopplerBin, currentSequencePhase, chipIndex;
    float currentMagnitude, bestMagnitude, meanMagnitude, currentFrequencyOffset;
    float complex accumulator;

    // Generate the PRN sequence we'll be using.
    gps_generatePRN(prnSequence, prn);

    bestMagnitude = 0.0f;
    meanMagnitude = 0.0f;

    // Enumerate all Doppler offsets and sequence phases.
    for (dopplerBin = 0; dopplerBin < RX_DOPPLER_BINS; dopplerBin++) {
        currentFrequencyOffset = (dopplerBin - RX_DOPPLER_BINS / 2) * RX_DOPPLER_BIN_WIDTH;

        for (currentSequencePhase = 0; currentSequencePhase < RX_WINDOW_LENGTH; currentSequencePhase++) {
            // Correlation.
            accumulator = 0.0f;
            for (i = 0; i < RX_EVALUATION_WINDOW_LENGTH; i += RX_SAMPLES_PER_CHIP) {
                // Index of the current chip.
                chipIndex = ((i + currentSequencePhase) % RX_EVALUATION_WINDOW_LENGTH) / RX_SAMPLES_PER_CHIP % GPS_SEQUENCE_LENGTH;

                // Rotate the sample and add it to the accumulator.
                accumulator += window[i] * prnSequence[chipIndex] * rotationTable[dopplerBin * RX_EVALUATION_WINDOW_LENGTH + i];
            }

            // Find the current magnitude.
            currentMagnitude = cabsf(accumulator);

            // Add to the mean.
            meanMagnitude += currentMagnitude;

            // New best.
            if (currentMagnitude > bestMagnitude) {
                bestMagnitude = currentMagnitude;

                // Update the output values.
                *frequencyOffset = currentFrequencyOffset;
                *sequencePhase = currentSequencePhase;
            }
        }
    }

    // Divide by the total count of entries.
    meanMagnitude /= RX_DOPPLER_BINS * RX_WINDOW_LENGTH;

    // The mean is now complete, so we can compute the signal strength.
    *signalStrength = bestMagnitude / meanMagnitude;
}


// Initialize the channel to an idle state).
void gps_rx_channel_init(gps_rx_channel_t *channel, gps_rx_subframeCallback *subframeCallback) {
    channel->subframeCallback = subframeCallback;
    channel->sampleCounter = 0;
    gps_rx_channel_disable(channel);
}

// Stop receiving on a given channel.
void gps_rx_channel_disable(gps_rx_channel_t *channel) {
    channel->prn = RX_DUMMY_PRN;
    channel->frameSync = 0;
}

// Reset the channel.
void gps_rx_channel_reset(gps_rx_channel_t *channel, int prn, float initialFrequencyOffset, int initialPhaseOffset) {
    int i;
    channel->prn = prn;

    // Dummy, no further initialization necessary.
    if (prn == RX_DUMMY_PRN)
        return;

    channel->frequencyOffset = initialFrequencyOffset;
    channel->syncCounter = initialPhaseOffset; // Where we are in the symbol cycle right now, supposedly.
    channel->syncOffset = 0; // Nominally zero.
    channel->rotationPhase = 0.0f;

    gps_generatePRN(channel->prnSequence, prn);
    for (i = 0; i < RX_WINDOW_LENGTH; i++)
        channel->correlationWindow[i] = 0.0f;
    channel->windowIndex = 0;
    channel->syncEarlyMagnitude = 0.0f;
    channel->syncLateMagnitude = 0.0f;
    channel->syncCenterMagnitude = 0.0f;
    channel->peakSample = 0.0f;

    // This has to be a multiple of the window length so that the initial phase offset estimate remains valid.
    channel->skipCounter = RX_WINDOW_LENGTH;

    channel->syncSkipAdjustment = 1;

    channel->frameSync = 0;
    for (i = 0; i < GPS_SEQUENCES_PER_BIT; i++)
        channel->symbolAverageWindow[i] = 0.0f;
    channel->symbolAverageIndex = 0;
    channel->bitClockCounter = 0;
    channel->syncBitClockPrevPeakReal = 0.0f;
    channel->wordShift = 0;
    channel->wordBitCounter = 0;

    channel->parityErrorCounter = 0;
    channel->syncPrevBitClockCounter = -1;
    channel->syncEdgeRepeatCounter = 0;
    channel->subframeWordCounter = 0;
}

// Phase error for a BPSK symbol.
float _phaseError(float complex x) {
    return crealf(x) >= 0.0f ? cargf(x) : cargf(-x);
}

// Feed samples.
void gps_rx_channel_input(gps_rx_channel_t *channel, float complex *samples, int numSamples) {
    int i, j, earlyOffset, lateOffset, postLateOffset, trackingPeriod, phaseChanged, bitLevel, subframeValid, decodedWord;
    float complex accumulator;
    float correlationMagnitude, phaseError, symbolAverageAccumulator;
    gps_subframe_t subframe;

    // Don't do anything if the channel isn't used.
    if (channel->prn == RX_DUMMY_PRN) {
        // Increment the sample counter to catch up with the other channels.
        channel->sampleCounter += numSamples;
        return;
    }

    for (i = 0; i < numSamples; i++) {
        // Offsets for comparison.
        earlyOffset = (channel->syncOffset - 1 + RX_WINDOW_LENGTH) % RX_WINDOW_LENGTH;
        lateOffset = (channel->syncOffset + 1) % RX_WINDOW_LENGTH;

        // This is to ensure that our point won't be skipped over even if sync corrections are performed.
        postLateOffset = (channel->syncOffset + 3) % RX_WINDOW_LENGTH;
        trackingPeriod = channel->syncCounter == channel->syncOffset || channel->syncCounter == earlyOffset || channel->syncCounter == lateOffset;

        // Place the sample in the correlation window.
        channel->correlationWindow[channel->windowIndex++] = samples[i] * cexpf(-I * channel->rotationPhase * M_PI * 2);
        channel->windowIndex %= RX_WINDOW_LENGTH;

        // Skip some samples initially so that the delay line fills.
        if (channel->skipCounter) {
            channel->skipCounter--;

            // Increment the sample count, this is very important.
            channel->sampleCounter++;
            continue;
        }

        // Update the rotation phase.
        channel->rotationPhase = fmodf(channel->rotationPhase + channel->frequencyOffset / RX_RATE, 1.0f);

        // We only perform correlation during the tracking period (early, center, late samples).
        if (trackingPeriod) {
            // Correlation.
            accumulator = 0.0f;
            for (j = 0; j < RX_WINDOW_LENGTH; j += RX_SAMPLES_PER_CHIP)
                accumulator += channel->correlationWindow[(channel->windowIndex + j) % RX_WINDOW_LENGTH] * channel->prnSequence[j / RX_SAMPLES_PER_CHIP];

            correlationMagnitude = cabsf(accumulator);

            // Early sample.
            if (channel->syncCounter == earlyOffset) {
                channel->syncEarlyMagnitude = correlationMagnitude;
            } else if (channel->syncCounter == channel->syncOffset) {
                channel->syncCenterMagnitude = correlationMagnitude;
                channel->peakSample = accumulator;
            } else if (channel->syncCounter == lateOffset) {
                channel->syncLateMagnitude = correlationMagnitude;

                if (!channel->syncSkipAdjustment) {
                    // Log.
                    //log_printf("PRN %d: sync at %d (%.2f %.2f %.2f)\n", channel->prn, channel->syncOffset, channel->syncEarlyMagnitude, channel->syncCenterMagnitude, channel->syncLateMagnitude);

                    if (channel->syncEarlyMagnitude > channel->syncCenterMagnitude && channel->syncEarlyMagnitude > channel->syncLateMagnitude) {
                        // Too early.
                        channel->syncOffset = (channel->syncOffset - 1 + RX_WINDOW_LENGTH) % RX_WINDOW_LENGTH;
                        channel->syncSkipAdjustment = 1;
                    } else if (channel->syncLateMagnitude > channel->syncCenterMagnitude && channel->syncLateMagnitude > channel->syncEarlyMagnitude) {
                        // Too late.
                        channel->syncOffset = (channel->syncOffset + 1) % RX_WINDOW_LENGTH;
                        channel->syncSkipAdjustment = 1;
                    }

                    // Compute the phase and frequency errors.
                    phaseError = _phaseError(channel->peakSample);

                    // Log.
                    // DEBUG
                    if (channel->prn == DEBUG_PRN)
                    log_printf("PRN %d: sync offset %d, peak %f, frequency %.2f, phaseError %.2f, symbol %f %f (bit clock %d)\n", channel->prn, channel->syncOffset, channel->syncCenterMagnitude, channel->frequencyOffset, phaseError, crealf(channel->peakSample), cimagf(channel->peakSample), channel->bitClockCounter);

                    // Update the phase and the frequency.
                    channel->rotationPhase += phaseError * RX_PHASE_COEFF;
                    channel->frequencyOffset += phaseError * RX_FREQUENCY_COEFF;
                } else if (channel->syncCounter == lateOffset) {
                    // Reset sync counter adjustment lock.
                    channel->syncSkipAdjustment = 0;
                }
            }
        }

        // Bit sampling.
        if (channel->syncCounter == postLateOffset) {
            // Detect phase changes.
            phaseChanged = crealf(channel->peakSample) * channel->syncBitClockPrevPeakReal < 0.0f;
            channel->syncBitClockPrevPeakReal = crealf(channel->peakSample);

            // Reset the clock when a symbol edge is encountered.
            if (!channel->frameSync && phaseChanged) {
                if (channel->bitClockCounter == channel->syncPrevBitClockCounter) {
                    if (++channel->syncEdgeRepeatCounter == RX_SYNC_EDGE_REPEAT_THRESHOLD) {
                        log_printf("PRN %d: symbol edge at %d (%f)\n", channel->prn, channel->bitClockCounter, crealf(channel->peakSample));
                        channel->bitClockCounter = 0;
                        channel->syncEdgeRepeatCounter = 0;
                    }
                } else {
                    channel->syncEdgeRepeatCounter = 0;
                }
                channel->syncPrevBitClockCounter = channel->bitClockCounter;
            }

            // Sampling point?
            if (channel->bitClockCounter == 0) {
                // Compute the moving average.
                symbolAverageAccumulator = 0.0f;
                for (j = 0; j < GPS_SEQUENCES_PER_BIT; j++)
                    symbolAverageAccumulator += channel->symbolAverageWindow[j];

                //log_printf("PRN %d: symbol acc %f\n", channel->prn, symbolAverageAccumulator);
                bitLevel = symbolAverageAccumulator > 0.0f ? 1 : 0;

                // Shift it in.
                channel->wordShift = (channel->wordShift << 1 | bitLevel) & 0xffffffff;

                // Test for a valid word and a TLM preamble.
                if (!channel->frameSync) {
                    // NOTE: This seems to work even if the input bits are inverted. Neat.
                    if ((decodedWord = gps_decodeWord(channel->wordShift)) != -1) {
                        //log_printf("PRN %d: awaiting TLM, word %06x (%08x; last bit %f)\n", channel->prn, decodedWord, channel->wordShift, symbolAverageAccumulator);
                        //log_printf("PRN %d: sync offset %d, peak %f, frequency %.2f, phaseError %.2f, symbol %f %f\n", channel->prn, channel->syncOffset, channel->syncCenterMagnitude, channel->frequencyOffset, _phaseError(channel->peakSample), crealf(channel->peakSample), cimagf(channel->peakSample));
                        if (decodedWord >> 16 == GPS_SUBFRAME_PREAMBLE) {
                            log_printf("PRN %d: frame sync acquired\n", channel->prn);
                            channel->frameSync = 1;
                            channel->subframeWordCounter = 0;

                            // Store the initial TLM word.
                            channel->subframeWords[channel->subframeWordCounter++] = decodedWord;
                        }
                    }
                } else {
                    // Every 30 bits.
                    if (++channel->wordBitCounter == GPS_BITS_PER_WORD) {
                        channel->wordBitCounter = 0;
                        if ((decodedWord = gps_decodeWord(channel->wordShift)) != -1) {
                            channel->parityErrorCounter = 0;
                            log_printf("PRN %d: data word %08x, decoded %06x (at %d)\n", channel->prn, channel->wordShift, decodedWord, channel->subframeWordCounter);
                            log_printf("PRN %d: sample counter since init %lld\n", channel->prn, channel->sampleCounter);
                            log_printf("PRN %d: sync offset %d, peak %f, frequency %.2f, phaseError %.2f, symbol %f %f\n", channel->prn, channel->syncOffset, channel->syncCenterMagnitude, channel->frequencyOffset, _phaseError(channel->peakSample), crealf(channel->peakSample), cimagf(channel->peakSample));
                        } else {
                            log_printf("PRN %d: data error! (%d)\n", channel->prn, channel->parityErrorCounter);

                            // Too many errors in a row?
                            if (++channel->parityErrorCounter == RX_PARITY_ERROR_THRESHOLD) {
                                log_printf("PRN %d: sync lost\n", channel->prn);

                                // Reset so that it can be detected again.
                                channel->parityErrorCounter = 0;
                                channel->frameSync = 0;
                            }
                        }

                        // Store the subframe word.
                        channel->subframeWords[channel->subframeWordCounter] = decodedWord;

                        // Full subframe accumulated?
                        if (++channel->subframeWordCounter == GPS_WORDS_PER_SUBFRAME) {
                            channel->subframeWordCounter = 0;

                            // Test whether all words are present.
                            subframeValid = 1;
                            for (j = 0; j < GPS_WORDS_PER_SUBFRAME; j++) {
                                if (channel->subframeWords[j] == -1) {
                                    subframeValid = 0;
                                    break;
                                }
                            }

                            log_printf("PRN %d: subframe, valid=%d\n", channel->prn, subframeValid);
                            if (subframeValid) {
                                gps_decodeSubframe(&subframe, channel->subframeWords);
                                log_printf("-> %d, %d\n", subframe.towTruncated, subframe.subframeId);

                                // If we've got a callback, invoke it.
                                if (channel->subframeCallback)
                                    channel->subframeCallback(channel->prn, subframe, channel->sampleCounter);
                            }
                        }
                    }
                }
            }

            // Update the moving average window.
            channel->symbolAverageWindow[channel->symbolAverageIndex++] = crealf(channel->peakSample);
            channel->symbolAverageIndex %= GPS_SEQUENCES_PER_BIT;

            // Increment the bit clock counter.
            channel->bitClockCounter++;
            channel->bitClockCounter %= GPS_SEQUENCES_PER_BIT;
        }

        // Update the synchronization counter.
        if (++channel->syncCounter == RX_WINDOW_LENGTH)
            channel->syncCounter = 0;

        // Increment the sample counter.
        channel->sampleCounter++;
    }
}

// How many samples were processed since the channel was initialized?
long long gps_rx_channel_getSamplesSinceInit(gps_rx_channel_t *channel) {
    return channel->sampleCounter;
}

// Does the channel have frame sync?
int gps_rx_channel_getFrameSync(gps_rx_channel_t *channel) {
    return channel->frameSync;
}
