#ifndef EPHEMERIS_H
#define EPHEMERIS_H
#include "gps.h"
#include "navigation.h"

// Chosen empirically, should be sufficient.
#define EPHEMERIS_KEPLER_ITERATIONS 15

int gps_ephemeris_findPosition(ECEF_coords_t *ecefCoords, gps_subframe_t *subframe2, gps_subframe_t *subframe3, double weekSeconds);
#endif
