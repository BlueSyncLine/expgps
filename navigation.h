#ifndef NAVIGATION_H
#define NAVIGATION_H

// Empirically chosen.
#define NAVIGATION_NUM_ITERATIONS 10000

// Empirically chosen, can cause the solution to jump to an incorrect value if too large.
#define NAVIGATION_CORRECTION_COEFF 0.1

// At least three satellites are required to solve for location.
#define NAVIGATION_MIN_SATELLITES 3

typedef struct {
    double x;
    double y;
    double z;
} ECEF_coords_t;

typedef struct {
    double latitude;
    double longitude;
    double altitude;
} LLA_coords_t;

typedef struct {
    ECEF_coords_t coords;
    double timeOffset;
} gps_navigation_satellitePosition_t;

typedef struct {
    ECEF_coords_t coords;
    double clockBiasMeters;
} gps_navigation_solution_t;

void gps_navigation_ECEFtoLLA(LLA_coords_t *lla, ECEF_coords_t *ecef);
int gps_navigation_solve(gps_navigation_solution_t *solution, gps_navigation_satellitePosition_t *satellites, int numSatellites);
#endif
