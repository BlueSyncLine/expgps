#include <math.h>
#include "fir.h"
#include "log.h"

// Sinc function.
float _sinc(float x) {
    return x == 0.0f ? 1.0f : sinf(x) / x;
}

// Blackman window.
float _blackman(int M, int n) {
    return 0.42f - 0.5f * cosf(M_PI * 2.0f * n / M) + 0.08f * cosf(M_PI * 4.0f * n / M);
}

// Compute the taps for a FIR lowpass filter.
void fir_lowpass_taps(float *taps, int n, float fc) {
    int i, j, numTaps;
    float acc;

    // Total taps.
    numTaps = 2 * n + 1;

    // Compute the taps.
    j = 0;
    acc = 0.0f;
    for (i = -n; i <= n; i++) {
        taps[j] = _sinc(M_PI * 2.0f * i * fc) * _blackman(numTaps + 1, j + 1);
        acc += taps[j];
        j++;
    }

    // Normalize.
    for (i = 0; i < numTaps; i++)
        taps[i] /= acc;

    log_printf("fir_lowpass_taps: n=%d, fc=%f\n", n, fc);
    for (i = 0; i < numTaps; i++)
        log_printf("%f, ", taps[i]);
    log_printf("\n");
}
