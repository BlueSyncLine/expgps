#include <math.h>
#include "gps.h"
#include "navigation.h"
#include "ephemeris.h"
#include "wgs84.h"

// DEBUG
#include "log.h"

#define SEMICIRCLES_TO_RADIANS(x) (x * M_PI)

// Solve Kepler's equation.
double keplerSolve(double meanAnomaly, double eccentricity) {
    int i;
    double eccentricAnomaly;

    // Make sure it's in range.
    meanAnomaly = fmod(meanAnomaly, M_PI * 2);

    // First estimate.
    eccentricAnomaly = meanAnomaly;

    // Iterate.
    for (i = 0; i < EPHEMERIS_KEPLER_ITERATIONS; i++)
        eccentricAnomaly = meanAnomaly + eccentricity * sin(eccentricAnomaly);

    return eccentricAnomaly;
}

// Computes the satellite position from the ephemeris at a given time.
// 0 is returned on error, 1 on success.
// IS-GPS-200L, Table 20-IV.
int gps_ephemeris_findPosition(ECEF_coords_t *ecefCoords, gps_subframe_t *subframe2, gps_subframe_t *subframe3, double weekSeconds) {
    double a, n_0, t_k, n, m_k, e_k, v_k, argLat_k, d_u_k, d_r_k, d_i_k, u_k, r_k, i_k, xx_k, yy_k, an_k;

    // Check the ephemeris for basic correctness criteria.
    if (subframe2->subframeId != GPS_SUBFRAME_2 || subframe3->subframeId != GPS_SUBFRAME_3 || subframe2->fields.subframe2.iode != subframe3->fields.subframe3.iode)
        return 0;

    // Semi-major axis.
    a = pow(subframe2->fields.subframe2.sqrt_a, 2.0);

    // Mean motion, rad/s.
    n_0 = sqrt(WGS84_EARTH_MU / pow(a, 3.0));

    // Compute the time difference and correct for week rollover.
    t_k = weekSeconds - subframe2->fields.subframe2.t_oe;
    if (t_k > 302400.0) {
        t_k -= 604800.0;
    } else if (t_k < -302400.0) {
        t_k += 604800.0;
    }

    // Corrected mean motion.
    n = n_0 + SEMICIRCLES_TO_RADIANS(subframe2->fields.subframe2.delta_n);
    m_k = SEMICIRCLES_TO_RADIANS(subframe2->fields.subframe2.m_0) + n * t_k;

    // Solve for the eccentric anomaly.
    e_k = keplerSolve(m_k, subframe2->fields.subframe2.e);

    // True anomaly.
    v_k = 2.0 * atan(sqrt((1.0 + subframe2->fields.subframe2.e) / (1.0 - subframe2->fields.subframe2.e)) * tan(e_k / 2.0));

    // Argument of latitude.
    argLat_k = v_k + SEMICIRCLES_TO_RADIANS(subframe3->fields.subframe3.arg);

    // Second harmonic perturbations.
    d_u_k = subframe2->fields.subframe2.c_us * sin(2.0 * argLat_k) + subframe2->fields.subframe2.c_uc * cos(2.0 * argLat_k);
    d_r_k = subframe2->fields.subframe2.c_rs * sin(2.0 * argLat_k) + subframe3->fields.subframe3.c_rc * cos(2.0 * argLat_k);
    d_i_k = subframe3->fields.subframe3.c_is * sin(2.0 * argLat_k) + subframe3->fields.subframe3.c_ic * cos(2.0 * argLat_k);

    // Corrected argument of latitude.
    u_k = argLat_k + d_u_k;

    // Corrected radius.
    r_k = a * (1.0 - subframe2->fields.subframe2.e * cos(e_k)) + d_r_k;

    // Corrected inclination.
    i_k = SEMICIRCLES_TO_RADIANS(subframe3->fields.subframe3.i_0) + d_i_k + SEMICIRCLES_TO_RADIANS(subframe3->fields.subframe3.i_dot) * t_k;

    // Positions in orbital plane.
    xx_k = r_k * cos(u_k);
    yy_k = r_k * sin(u_k);

    // Corrected ascending node.
    an_k = SEMICIRCLES_TO_RADIANS(subframe3->fields.subframe3.an_0) + (SEMICIRCLES_TO_RADIANS(subframe3->fields.subframe3.an_dot) - WGS84_EARTH_ROTATION) * t_k - WGS84_EARTH_ROTATION * subframe2->fields.subframe2.t_oe;

    // Coordinates.
    ecefCoords->x = xx_k * cos(an_k) - yy_k * cos(i_k) * sin(an_k);
    ecefCoords->y = xx_k * sin(an_k) + yy_k * cos(i_k) * cos(an_k);
    ecefCoords->z = yy_k * sin(i_k);

    // Success.
    return 1;
}
