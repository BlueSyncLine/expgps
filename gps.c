#include <assert.h>
#include "gps.h"
#include "math.h"
#include "log.h"

// Polynomials for the shift registers.
#define GPS_POLY_G1 0x409
#define GPS_POLY_G2 0x74d

// GPS IS-200, Table 3-Ia.
const int GPS_PRN_G2_DELAYS[37] = {
    5, 6, 7, 8, 17, 18, 139, 140, 141, 251, 252, 254, 255, 256, 257, 258, 469, 470, 471, 472, 473, 474, 509, 512, 513, 514, 515, 516, 859, 860, 861, 862, 863, 950, 947, 948, 950
};

// Even parity bit for x.
int _parity(int x) {
    int p;

    p = 0;
    while (x) {
        p ^= x & 1;
        x >>= 1;
    }

    return p;
}

// Generates the chip sequence for a given PRN.
void gps_generatePRN(int *sequence, int prn) {
    int i, stateG1, stateG2, feedbackG1, feedbackG2;

    // Check the number for validity.
    assert(prn >= 1 && prn <= 37);

    // Initial states for the shift registers.
    stateG1 = 0x3ff;
    stateG2 = 0x3ff;

    // Delay G2 by advancing it to the desired start point.
    for (i = 0; i < GPS_SEQUENCE_LENGTH - GPS_PRN_G2_DELAYS[prn - 1]; i++) {
        stateG2 <<= 1;
        stateG2 &= 0x7ff;
        feedbackG2 = _parity(stateG2 & GPS_POLY_G2);
        stateG2 |= feedbackG2;
    }

    // And now, produce the sequence.
    for (i = 0; i < GPS_SEQUENCE_LENGTH; i++) {
        // Advance the G1 shift register.
        stateG1 <<= 1;
        stateG1 &= 0x7ff;
        feedbackG1 = _parity(stateG1 & GPS_POLY_G1);
        stateG1 |= feedbackG1;

        // Advance the G2 shift register.
        stateG2 <<= 1;
        stateG2 &= 0x7ff;
        feedbackG2 = _parity(stateG2 & GPS_POLY_G2);
        stateG2 |= feedbackG2;

        // Store the resulting bit.
        sequence[i] = (stateG1 >> 10) ^ (stateG2 >> 10) ? 1 : -1;
    }

    /*log_printf("GPS: first 10 chips of PRN %d: ", prn);
    for (i = 0; i < 10; i++)
        log_printf("%d ", sequence[i]);
    log_printf("\n");*/
}

// Decodes a 32-bit word (two upper bits are the tail of the previous word of the subframe).
// Returns either the 24 bits or -1 if the parity check fails.
int gps_decodeWord(unsigned int word) {
    unsigned int dd30, x, p;
    // IS-GPS-200L, 20.3.5.2
    // Data is pre-XORed with the second-last parity bit of the previous frame.
    dd30 = (word >> 30) & 1;

    // Two bits of the previous and the current word without parity.
    x = word >> 6 ^ (dd30 ? 0xffffff : 0);

    // TODO check those.
    p = _parity(x & 0x2ec7cd2);
    p <<= 1;
    p |= _parity(x & 0x1763e69);
    p <<= 1;
    p |= _parity(x & 0x2bb1f34);
    p <<= 1;
    p |= _parity(x & 0x15d8f9a);
    p <<= 1;
    p |= _parity(x & 0x1aec7cd);
    p <<= 1;
    p |= _parity(x & 0x22dea27);

    if ((word & 0x3f) == p) {
        // If the parity is valid, return the word itself.
        return x & 0xffffff;
    } else {
        // Error.
        return -1;
    }
}

// Convert a fixed-point number to double.
// NOTE: it's very important to cast x to an unsigned int first.
double _fixedToDoubleUnsigned(unsigned int x, int power) {
    //log_printf("_fixedToDoubleUnsigned: %f\n", x * pow(2.0, power));
    return x * pow(2.0, power);
}

double _fixedToDoubleSigned(unsigned int x, int power, int numBits) {
    long long x64;

    // Extend two's complement to the full bit length.
    x64 = x;
    if (x64 & (1 << (numBits - 1)))
        x64 -= 1 << numBits;

    //log_printf("_fixedToDoubleSigned: %f\n", x64 * pow(2.0, power));
    return x64 * pow(2.0, power);
}

// Decodes a given subframe.
void gps_decodeSubframe(gps_subframe_t *subframe, int words[GPS_WORDS_PER_SUBFRAME]) {
    unsigned int *unsignedWords;
    unsignedWords = (unsigned int*)words;

    // Unpack the bit fields.
    subframe->integrityStatusFlag = (unsignedWords[0] >> 1) & 1;
    subframe->towTruncated = unsignedWords[1] >> 7;
    subframe->alertFlag = (unsignedWords[1] >> 6) & 1;
    subframe->antiSpoofFlag = (unsignedWords[1] >> 5) & 1;
    subframe->subframeId = (unsignedWords[1] >> 2) & 7;

    // Decode fields based on the subframe ID.
    switch (subframe->subframeId) {
        case GPS_SUBFRAME_1:
            subframe->fields.subframe1.weekNumber = unsignedWords[2] >> 14;
            subframe->fields.subframe1.l2Code = (unsignedWords[2] >> 12) & 3;
            subframe->fields.subframe1.uraIndex = (unsignedWords[2] >> 8) & 0xf;
            subframe->fields.subframe1.svHealth = (unsignedWords[2] >> 2) & 0x3f;
            subframe->fields.subframe1.iodc = (unsignedWords[2] & 3) << 10 | (unsignedWords[7] >> 16);
            subframe->fields.subframe1.l2PFlag = unsignedWords[3] >> 23;
            subframe->fields.subframe1.t_gd = _fixedToDoubleSigned(unsignedWords[6] & 0xff, -31, 8);
            subframe->fields.subframe1.t_oc = _fixedToDoubleUnsigned(unsignedWords[7] & 0xffff, 4);
            subframe->fields.subframe1.a_f2 = _fixedToDoubleSigned(unsignedWords[8] >> 16, -55, 8);
            subframe->fields.subframe1.a_f1 = _fixedToDoubleSigned(unsignedWords[8] & 0xffff, -43, 16);
            subframe->fields.subframe1.a_f0 = _fixedToDoubleSigned(unsignedWords[9] >> 2, -31, 22);
            break;

        case GPS_SUBFRAME_2:
            subframe->fields.subframe2.iode = unsignedWords[2] >> 16;
            subframe->fields.subframe2.c_rs = _fixedToDoubleSigned(unsignedWords[2] & 0xffff, -5, 16);
            subframe->fields.subframe2.delta_n = _fixedToDoubleSigned(unsignedWords[3] >> 8, -43, 16);
            subframe->fields.subframe2.m_0 = _fixedToDoubleSigned((unsignedWords[3] & 0xff) << 24 | unsignedWords[4], -31, 32);
            subframe->fields.subframe2.c_uc = _fixedToDoubleSigned(unsignedWords[5] >> 8, -29, 16);
            subframe->fields.subframe2.e = _fixedToDoubleUnsigned((unsignedWords[5] & 0xff) << 24 | unsignedWords[6], -33);
            subframe->fields.subframe2.c_us = _fixedToDoubleSigned(unsignedWords[7] >> 8, -29, 16);
            subframe->fields.subframe2.sqrt_a = _fixedToDoubleUnsigned((unsignedWords[7] & 0xff) << 24 | unsignedWords[8], -19);
            subframe->fields.subframe2.t_oe = _fixedToDoubleUnsigned(unsignedWords[9] >> 8, 4);
            subframe->fields.subframe2.fitIntervalFlag = (unsignedWords[9] >> 7) & 1;
            subframe->fields.subframe2.aodo = (unsignedWords[9] >> 2) & 0x1f;
            break;

        case GPS_SUBFRAME_3:
            subframe->fields.subframe3.c_ic = _fixedToDoubleSigned(unsignedWords[2] >> 8, -29, 16);
            subframe->fields.subframe3.an_0 = _fixedToDoubleSigned((unsignedWords[2] & 0xff) << 24 | unsignedWords[3], -31, 32);
            subframe->fields.subframe3.c_is = _fixedToDoubleSigned(unsignedWords[4] >> 8, -29, 16);
            subframe->fields.subframe3.i_0 = _fixedToDoubleSigned((unsignedWords[4] & 0xff) << 24 | unsignedWords[5], -31, 32);
            subframe->fields.subframe3.c_rc = _fixedToDoubleSigned(unsignedWords[6] >> 8, -5, 16);
            subframe->fields.subframe3.arg = _fixedToDoubleSigned((unsignedWords[6] & 0xff) << 24 | unsignedWords[7], -31, 32);
            subframe->fields.subframe3.an_dot = _fixedToDoubleSigned(unsignedWords[8], -43, 24);
            subframe->fields.subframe3.iode = unsignedWords[9] >> 16;
            subframe->fields.subframe3.i_dot = _fixedToDoubleSigned((unsignedWords[9] >> 2) & 0x3fff, -43, 14);

        // TODO implement
        case GPS_SUBFRAME_4:
        case GPS_SUBFRAME_5:
            break;

        default:
            subframe->subframeId = GPS_SUBFRAME_INVALID;
            break;
    }
}

// Perform time correction.
// IS-200L, 20.3.3.3.3.1
int gps_findTimeCorrection(double *correction, gps_subframe_t *subframe1) {
    double sinceEpoch;

    // Make sure it's indeed subframe 1.
    if (subframe1->subframeId != GPS_SUBFRAME_1)
        return 0;

    sinceEpoch = subframe1->towTruncated * GPS_SECONDS_PER_TOW - subframe1->fields.subframe1.t_oc;

    // Horner's method.
    *correction = subframe1->fields.subframe1.a_f0 + (subframe1->fields.subframe1.a_f1 + (subframe1->fields.subframe1.a_f2 * sinceEpoch)) * sinceEpoch;
    return 1;
}
