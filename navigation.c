#include <math.h>
#include <stdlib.h>
#include "wgs84.h"
#include "navigation.h"
#include "log.h"

// Source: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#The_application_of_Ferrari%27s_solution.
void gps_navigation_ECEFtoLLA(LLA_coords_t *lla, ECEF_coords_t *ecef) {
    double r, f, g, c, s, p, q, r_0, u, v, z_0;

    r = hypot(ecef->x, ecef->y);

    // Prevent division by zero.
    if (r == 0.0)
        r = 1.0;

    f = 54.0 * pow(WGS84_EARTH_B, 2.0) * pow(ecef->z, 2.0);
    g = pow(r, 2.0) + (1.0 - WGS84_EARTH_E_SQUARED) * pow(ecef->z, 2.0) - WGS84_EARTH_E_SQUARED * (pow(WGS84_EARTH_A, 2.0) - pow(WGS84_EARTH_B, 2.0));
    c = pow(WGS84_EARTH_E_SQUARED, 2.0) * f * pow(r, 2.0) / pow(g, 3.0);
    s = cbrt(1.0 + c + sqrt(pow(c, 2.0) + 2.0 * c));
    p = f / (3.0 * pow(s + 1.0 + 1.0 / s, 2.0) * pow(g, 2.0));
    q = sqrt(1.0 + 2.0 * pow(WGS84_EARTH_E_SQUARED, 2.0) * p);
    r_0 = -p * WGS84_EARTH_E_SQUARED * r / (1.0 + q) + sqrt(0.5 * pow(WGS84_EARTH_A, 2.0) * (1.0 + 1.0 / q) - p * (1.0 - WGS84_EARTH_E_SQUARED) * pow(ecef->z, 2.0) / (q * (1.0 + q)) - 0.5 * p * pow(r, 2.0));
    u = hypot(r - WGS84_EARTH_E_SQUARED * r_0, ecef->z);
    v = sqrt(pow(r - WGS84_EARTH_E_SQUARED * r_0, 2.0) + (1.0 - WGS84_EARTH_E_SQUARED) * pow(ecef->z, 2.0));
    z_0 = pow(WGS84_EARTH_B, 2.0) * ecef->z / (WGS84_EARTH_A * v);
    lla->altitude = u * (1.0 - pow(WGS84_EARTH_B, 2.0) / (WGS84_EARTH_A * v));
    lla->latitude = atan((ecef->z + WGS84_EARTH_E_PRIME_SQUARED * z_0) / r) * 360.0 / (M_PI * 2.0);
    lla->longitude = atan2(ecef->y, ecef->x) * 360.0 / (M_PI * 2.0);
}

// Distance between two 3D points.
double _distance3D(ECEF_coords_t *a, ECEF_coords_t *b) {
    return sqrt(pow(b->x - a->x, 2.0) + pow(b->y - a->y, 2.0) + pow(b->z - a->z, 2.0));
}

// Total squared position error.
double _totalSquaredError(gps_navigation_solution_t *estimate, gps_navigation_satellitePosition_t *satellites, int numSatellites, int twoDimensionalFix) {
    int i;
    double accumulator, pseudorange, distance;
    LLA_coords_t lla;

    accumulator = 0.0;

    for (i = 0; i < numSatellites; i++) {
        pseudorange = satellites[i].timeOffset * WGS84_SPEED_OF_LIGHT - estimate->clockBiasMeters;
        distance = _distance3D(&estimate->coords, &satellites[i].coords);
        accumulator += pow(pseudorange - distance, 2.0);
    }

    // 2D fix mode, make sure the coordinates lie exactly on the surface by computing the true altitude.
    if (twoDimensionalFix) {
        // Convert the current ECEF estimate to LLA coordinates.
        gps_navigation_ECEFtoLLA(&lla, &estimate->coords);

        // Add to the error accumulator.
        accumulator += pow(lla.altitude, 2.0);
    }

    return accumulator;
}

// Random double in range -1 ... 1.
double _random() {
    return (double)rand() / RAND_MAX * 2.0 - 1.0;
}

// A shitty but seemingly operable means of solving the navigation equation.
int gps_navigation_solve(gps_navigation_solution_t *solution, gps_navigation_satellitePosition_t *satellites, int numSatellites) {
    int i, twoDimensionalFix;
    double error, newError, correctionMagnitude;
    gps_navigation_solution_t candidate;

    // Initialize the target coordinates.
    solution->coords.x = 0.0;
    solution->coords.y = 0.0;
    solution->coords.z = 0.0;
    solution->clockBiasMeters = 0.0;

    // Not enough satellites.
    if (numSatellites < NAVIGATION_MIN_SATELLITES)
        return 0;

    // We'll have to correct for altitude if there's less than four satellites.
    twoDimensionalFix = numSatellites < 4;

    // Iterate.
    for (i = 0; i < NAVIGATION_NUM_ITERATIONS; i++) {
        error = _totalSquaredError(solution, satellites, numSatellites, twoDimensionalFix);

        // Add a random vector.
        candidate = *solution;
        correctionMagnitude = sqrt(error) * NAVIGATION_CORRECTION_COEFF;
        candidate.coords.x += _random() * correctionMagnitude;
        candidate.coords.y += _random() * correctionMagnitude;
        candidate.coords.z += _random() * correctionMagnitude;
        candidate.clockBiasMeters += _random() * correctionMagnitude;

        // Compute the new error.
        newError = _totalSquaredError(&candidate, satellites, numSatellites, twoDimensionalFix);
        //log_printf("solve: new error: %e\n", newError);

        // The current vector is better.
        if (newError < error)
            *solution = candidate;
    }
    log_printf("solve: last error: %e\n", error);

    // Success.
    return 1;
}
